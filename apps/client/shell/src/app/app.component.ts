import { Component } from '@angular/core';

@Component({
  selector: 'adopet-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'client-shell';
}
