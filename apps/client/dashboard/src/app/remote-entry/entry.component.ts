import { Component } from '@angular/core';

@Component({
  selector: 'adopet-client-dashboard-entry',
  template: `<adopet-nx-welcome></adopet-nx-welcome>`,
})
export class RemoteEntryComponent {}
